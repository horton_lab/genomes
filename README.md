### Analyzing the Fragaria genome

This package contains code for the preparation and management of genomic polymorphism data. 
The current version is 1.0

### The main dependencies are:
* For whole-genome analyses: GATK4
* For GBS: Tassel, GB-eaSy, or both
* For coverage, ..., : vcftools
* For PCA: eigensoft/smartpca
* R version: 
* Python version: 

### Configuring the environment
The R scripts use the command _Sys.getenv()_ to load environmental variables.  
The Python scripts use the command os.environ.get() to load environmental variables.

You will need to update the variables in the file .Renviron (main folder) to suit your environment.  
*conda users: the yaml file and environmental variable scripts are under the conda_files directory.

### Contact
Matt Horton  
horton.matthew.w@gmail.com

### License
This project is licensed under the terms of the MIT license.
