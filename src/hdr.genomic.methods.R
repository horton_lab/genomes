# various functions, including
# * extract/convert VCF files to plink format
# * prepare files for smartpca (as in Horton et al., 2012)
# * create ibs matrices (similarity and dissimilarity)
#
# TODO: add r2 values to the filenames for greater flexibility in file loading (in progress)
#
# Author: Matt Horton
###############################################################################

subsetVcf <- function( outputDirectory, outputName, requestedSamples, vcf="gdb_survey_filtsnps.vcf", firmOnDropouts=TRUE ){

	## the requested file type has to be the same as the input... (currently)
	stopifnot(all.equal(tools::file_ext(vcf), tools::file_ext(outputName)));

	###############################################################################
	## if the output dir wasn't specified, check the outputName...
	###############################################################################
	if( missing(outputDirectory) & ( dirname(outputName) != outputName )){
		stopifnot(dir.exists(dirname(outputName)));
		outputDirectory <- dirname(outputName);
		cat("Setting the output directory to: ", outputDirectory, "\n");
	}

	###############################################################################
	## sanity check: did the user ask for samples in the vcf? :)
	###############################################################################
	command <- paste0( "bcftools query -l ", vcf );
	result <- system( command, intern=T, wait=T );
	sampleNames <- unlist(strsplit( result, "\n" ));

	dropouts <- which( !requestedSamples %in% sampleNames );
	if( length(dropouts) > 0 & firmOnDropouts ){
		cat("One or more of the samples that you requested could not be found:\n" );
		cat( paste0( sort(requestedSamples[dropouts]), collapse=", "), "\n");
		stop("Error.\n");
	}

	sampleNames <- paste0( intersect( sampleNames, requestedSamples ), collapse="," );

	extension <- tools::file_ext( vcf );
	if( extension == "vcf" ){
		command <- paste0( "vcf-subset -c ", sampleNames, " ", vcf, " > ", outputDirectory, "/", basename( outputName ));

	} else if( extension == "gz" ){
		command <- paste0( "bcftools view -Oz -s ", sampleNames, " ", vcf, " > ", outputDirectory, "/", basename( outputName ));
	}

	result <- system( command, intern=T, wait=T );
	stopifnot( result == 0 );

	return(result);
}

###############################################################################
## use vcftools to extract SNPs
###############################################################################
convertVcfToPlink <- function( outputDirectory, outputName, vcf="gdb_survey_filtsnps.vcf", minDepth=2, proportionMissingData=0, dropCols=TRUE ){

	if( missing(outputDirectory) & ( dirname(outputName) != outputName )){
		stopifnot(dir.exists(dirname(outputName)));
		outputDirectory <- dirname(outputName);
		cat("Setting the output directory to: ", outputDirectory, "\n");
	}

	if( !is.character( outputName )){
		outputName <- paste0( "out_", gsub( ".vcf", "", vcf ), "_miss", proportionMissingData);
	}
	
	command <- paste0( "vcftools ", ifelse( tools::file_ext( vcf ) == "vcf", " --vcf ", " --gzvcf "), vcf, 
			" --min-alleles 2",
			ifelse( !is.na( minDepth ), paste0( " --min-meanDP ", minDepth ), ""),
			" --max-meanDP 50", ## use a --max-meanDP flag to avoid possible duplicates.
			" --plink", ## below, we thin with plink
			" --max-missing ", (1 - proportionMissingData ), 
			" --out ", outputDirectory, "/", basename( outputName ));
	
	result <- system(command, wait=T, intern=T);
	stopifnot(result == 0);

	###############################################################################
	## create the fam/indiv file
	###############################################################################
	fileName <- paste0( outputDirectory, "/", basename( outputName ));
	createFamFileCommand <- paste0( "cut -f 1-6 ",  fileName, ".ped", " > tmp && mv tmp ", fileName, ".indiv" );
	result <- system(createFamFileCommand, wait=T, intern=T);
	stopifnot(result == 0);

	###############################################################################
	## optionally, drop the parents/sex/phenotype columns (cols 3-6)
	###############################################################################
	if( dropCols ){
		colCommand <- paste0( "cut -f -2,7- ", fileName, ".ped", " > tmp && mv tmp ", fileName, ".ped");
		result <- system(colCommand, wait=T, intern=T);
		stopifnot(result == 0);
	}

	return( list( filename=paste0( outputDirectory, "/", basename(outputName)), proportion=proportionMissingData ));
}

###############################################################################
## the chromosome names (Fvb1, etc.) in the *.vcfs files are a problem.
## extract them from the second column (when necessary) and fill the 1st col
## with those values...
###############################################################################
patchChromosomePlinkMap <- function( nameOfMapFile ){
	
	mapFile <- read.table( nameOfMapFile, header=F, sep="\t", as.is=T, stringsAsFactors=FALSE );
	mapFile[,1] <- gsub("^Fvb", "", do.call(rbind, strsplit( mapFile[,2], ":" ))[,1]);
	write.table( mapFile, nameOfMapFile, quote=F, sep="\t", row.names=F, col.names=F );

	return(mapFile);
}

###############################################################################
## prep the plink files and the smartpca parameters file
###############################################################################
prepareSmartPcaFiles <- function(
		prefix,
		r2=0.8,
		outputDir=paste(getwd(),"/",sep="")){

	commands <- "";

	###############################################################################
	## optional: reduce based on LD
	###############################################################################
	if( !is.na(r2)){
		getRidOfSNPsInLongRangeLD <- paste0(
				"--ped ", outputDir, "/", prefix, ".ped\n",
				"--map ", outputDir, "/", prefix, ".map\n",
				"--noweb\n",
				"--no-parents\n",
				"--no-sex\n",
				"--no-pheno\n",
				"--allow-no-sex\n",
				"\n",
				"--indep-pairwise 50 5 ", r2, "\n" ); ## to run, type: plink --script <filename>.r2.commands

		scriptName <- paste0( outputDir, "/", prefix, ".r2.commands");
		write.table(getRidOfSNPsInLongRangeLD, scriptName, quote=F, sep="\t", row.names=F, col.names=F);

		commands <- paste0( "plink --script ", scriptName, " > ", scriptName, ".log\n" );
	}
	
	###############################################################################
	## recode plink files
	###############################################################################
	pruneCommands <- paste0(
			"--ped ", outputDir, "/", prefix, ".ped\n",
			"--map ", outputDir, "/", prefix, ".map\n",
			"--noweb\n",
			"--no-parents\n",
			"--no-sex\n",
			"--no-pheno\n",
			"--allow-no-sex\n\n",
			ifelse( !is.na(r2), "--extract plink.prune.in\n", "" ),
			"--out ", outputDir, "/", prefix, ifelse( !is.na(r2), ".r2", ""), "\n\n",
			"--recode12\n" );

	scriptName <- paste0( outputDir, "/", prefix, ".recode.commands");
	write.table( pruneCommands, scriptName, quote=F, sep="\t", row.names=F, col.names=F );
	commands <- paste0( commands, "plink --script ", scriptName, " > ", scriptName, ".log\n" );
	
	###############################################################################
	## write out the par file parameters for smartpca
	###############################################################################
	parFileParameters <- paste0( 
			"genotypename:\t", outputDir, "/", prefix, ifelse(!is.na(r2), ".r2", ""), ".ped\n",
			"snpname:\t", outputDir, "/", prefix, ifelse(!is.na(r2), ".r2", ""), ".map\n",
			"indivname:\t", outputDir, "/", prefix, ".indiv\n",
			"evecoutname:\t", outputDir, "/", prefix, ".evec\n",
			"evaloutname:\t", outputDir, "/", prefix, ".eval\n",
			"altnormstyle:\tNO\n",
			"numoutevec:\t40\n",
			"lsqproject:\tYES\n" ); ## to run, type: smartpca -p myparfile.par 

	scriptName <- paste0( outputDir, "/", prefix, ".par");
	write.table( parFileParameters, scriptName, quote=F, sep="\t", row.names=F, col.names=F );
	commands <- paste0( commands, "smartpca -p ", scriptName, " > ", scriptName, ".log\n" );

	return( commands );
}

## remove whitespace at the front and end of the file; replace any remaining white spaces with tabs
tabify <- function( filename ){
	
	command <- paste0( "sed -e 's/^  *//;s/  *$//;s/  */\t/g' ", filename, " > tmp && mv tmp ", filename );
	return <- system( command, intern=T, wait=T);
	stopifnot( return == 0 );

	return( return );
}


createIbsMatrices <- function(
		prefix,
		r2=0.8,
		outputDir=paste(getwd(),"/",sep=""),
		dryRun=FALSE ){

	outputDir <- normalizePath( outputDir );
	
	###############################################################################
	## prepare a command to create a similarity matrix (suffix *.mibs)
	###############################################################################
	commands <- paste0( "plink --noweb --file ", prefix, ifelse( !is.na(r2), ".r2", "" ), " --cluster --matrix --out ", 
							outputDir, "/", basename( prefix ), " > ", outputDir, "/", basename( prefix ), ".ibs.log\n" );

	###############################################################################
	## now prepare a command to create a DISsimilarity matrix (suffix *.mdist)
	###############################################################################
	commands <- paste0( commands, 
			"plink --noweb --file ", prefix, ifelse( !is.na(r2), ".r2", "" ), " --cluster --distance-matrix --out ", 
							outputDir, "/", basename( prefix ), " > ", outputDir, "/", basename( prefix ), ".dist_ibs.log\n" ); 

	###############################################################################
	## optional sanity check: return the commands for user QC
	###############################################################################
	if( dryRun ){ return( commands ); }

	###############################################################################
	## write the bash script and run it. 
	###############################################################################
	qsubString <- paste0(
			"#!/bin/bash\n",
			commands );
	
	scriptName <- paste0( outputDir, "/ibs_script.sh");
	sink(scriptName);
	cat(qsubString);
	sink(NULL);

	results <- list();
	command <- paste0( "chmod u=rwx ", scriptName);
	results[[command]] <- system(command, intern=T, wait=T);
	stopifnot( results[[command]] == 0 );

	command <- paste0( "bash ", scriptName);
	timer <- system.time( results[[command]] <- system(command, intern=T, wait=T));
	stopifnot( results[[command]] == 0 );

	###############################################################################
	## swap white space for tabs
	###############################################################################
	## format *.mibs and *.mdist matrices
	sampleNames <- read.table( paste0( prefix, ".indiv"), header=F, sep="\t", as.is=T, stringsAsFactors=FALSE );

	matrixName <- paste0( outputDir, "/", basename(prefix), ".mibs" );
	stopifnot( file.exists( matrixName ));
	results[["mibs_file_tabs"]] <- tabify( matrixName );
	matrix <- read.table(matrixName, header=F, sep="\t", as.is=T);
	colnames(matrix) <- rownames(matrix) <- sampleNames[,1];
	write.table(matrix, matrixName, quote=F, sep="\t");
	
	matrixName <- paste0( outputDir, "/", basename(prefix), ".mdist" );
	stopifnot( file.exists( matrixName ));
	results[["mdist_file_tabs"]] <- tabify( matrixName );
	matrix <- read.table(matrixName, header=F, sep="\t", as.is=T);
	colnames(matrix) <- rownames(matrix) <- sampleNames[,1];
	write.table(matrix, matrixName, quote=F, sep="\t");
	
	cat("###############################################################################\n");
	cat( "The scripts for file:", prefix, "are complete.\n" ); print(timer);
	cat("###############################################################################\n");
	return( results );
}

###############################################################################
## use vcftools to estimate kinship
###############################################################################
vcfKinship <- function( outputDirectory, outputName, vcf="gdb_survey_filtsnps.vcf", minDepth=2, proportionMissingData=0, yang=FALSE ){ ## yang et al. 2010 or Manichakul 2010 (king-type) kinship
	
	if( missing(outputDirectory) & ( dirname(outputName) != outputName )){
		stopifnot(dir.exists(dirname(outputName)));
		outputDirectory <- dirname(outputName);
		cat("Setting the output directory to: ", outputDirectory, "\n");
	}
	
	if( !is.character( outputName )){
		outputName <- paste0( "out_", gsub( ".vcf", "", vcf ), "_miss", proportionMissingData);
	}
	
	command <- paste0( "vcftools ", ifelse( tools::file_ext( vcf ) == "vcf", " --vcf ", " --gzvcf "), vcf,
			" --min-alleles 2",
			ifelse( !is.na( minDepth ), paste0( " --min-meanDP ", minDepth ), ""),
			" --max-meanDP 50", ## use a --max-meanDP flag to help avoid the use of genomic duplicates.
			" --relatedness", ifelse( !yang, "2", "" ), ## append the number '2' to the relatedness argument if the caller wants a KinGship matrix (Manichakul et al., 2010)
			" --max-missing ", (1 - proportionMissingData ), ## yes, this argument has a counter-intuitive (opposite) name
			" --out ", outputDirectory, "/", basename( outputName ));
	
	result <- system(command, wait=T, intern=T);
	stopifnot(result == 0);
	
	return( list( filename=paste0( outputDirectory, "/", basename(outputName), ".relatedness", ifelse( !yang, "2", "")), proportion=proportionMissingData ));
}

###############################################################################
## convert the vcf file into a 012 format (the number of references alleles)
###############################################################################
vcf012 <- function( outputDirectory, outputName, vcf="gdb_survey_filtsnps.vcf", proportionMissingData=0 ){

	if( missing(outputDirectory) & ( dirname(outputName) != outputName )){
		stopifnot(dir.exists(dirname(outputName)));
		outputDirectory <- dirname(outputName);
		cat("Setting the output directory to: ", outputDirectory, "\n");
	}
	
	if( !is.character( outputName )){
		outputName <- paste0( "out_", gsub( ".vcf", "", vcf ), "_miss", proportionMissingData);
	}
	
	command <- paste0( "vcftools ", ifelse( tools::file_ext( vcf ) == "vcf", " --vcf ", " --gzvcf "), vcf,
			" --012",  
			" --max-missing ", (1 - proportionMissingData ), 
			" --out ", outputDirectory, "/", basename( outputName ));
	
	result <- system(command, wait=T, intern=T);
	stopifnot(result == 0);
	
	return( list( filename=paste0( outputDirectory, "/", basename(outputName), ".012" ), proportion=proportionMissingData ));
}

###############################################################################
## load the '012' files and perform PCA using base-R
###############################################################################
pca012 <- function( prefix ){

	###############################################################################
	## load the three 012 files
	###############################################################################
	snpFileName <- paste0( prefix, ".012" );
	stopifnot(file.exists( snpFileName ));
	snpFile <- read.table( snpFileName, header=F, sep="\t", as.is=T, stringsAsFactors=FALSE, row.names=1);
	is.na( snpFile[snpFile == -1] ) <- TRUE;

	snpPositionFileName <- paste0( sourceDirectory, "/", prefix, ".012.pos" );
	stopifnot(file.exists( snpPositionFileName ));
	positionFile <- read.table( snpPositionFileName, header=F, sep="\t", as.is=T, stringsAsFactors=FALSE );

	sampleFileName <- paste0( sourceDirectory, "/", prefix, ".012.indv" );
	stopifnot(file.exists( sampleFileName ));
	sampleFile <- read.table( sampleFileName, header=F, sep="\t", as.is=T, stringsAsFactors=FALSE );

	###############################################################################
	## 
	###############################################################################
	stopifnot(nrow(snpFile) == nrow(sampleFile));
	stopifnot(ncol(snpFile) == nrow( positionFile));
	rownames(snpFile) <- sampleFile[,1];

	colnames(positionFile) <- c("chr", "pos");
	snpFile <- data.frame( positionFile, t(snpFile), stringsAsFactors=FALSE );
	pca.robj <- prcomp( t(na.omit(snpFile[,-c(1,2)])), center=T, scale=T );

	return( list(snps=snpFile, positions=positionFile, samples=sampleFile, pca=pca.robj ));
}






