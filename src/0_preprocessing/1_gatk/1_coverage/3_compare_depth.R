# Spot check the genomic coverage...
# 
# Author: Matt Horton
###############################################################################

rm(list=ls());

require(ggplot2);

coverageFileName <- "microbiomeSurvey2017_150genomes.coverage_estimates.txt";
coverageFile <- read.table(coverageFileName, header=T, sep="\t", as.is=T, stringsAsFactors=F);

dep1 <- read.table("results/gdb_allsnps.idepth", header=T, sep="\t", as.is=T, stringsAsFactors=F);
dep2 <- read.table("results/gdb_allindels.idepth", header=T, sep="\t", as.is=T, stringsAsFactors=F);

colnames(dep1)[1] <- colnames(dep2)[1] <- "SampleID";
colnames(dep1)[2:3] <- gsub("$", "_snp", colnames(dep1)[2:3]);
colnames(dep2)[2:3] <- gsub("$", "_indel", colnames(dep2)[2:3]);

dep <- merge(dep1, dep2, by="SampleID");

coverageFile <- merge(coverageFile, dep, by="SampleID");
coverageFile <- within(coverageFile,
		{
			population <- substr(SampleID, 0, ( nchar(SampleID) - 1 ));
		});

g <- ggplot( coverageFile, aes(x=mean_coverage, y=MEAN_DEPTH_snp, colour=population)) + 
		theme_classic(base_size=20) +
		geom_point(alpha=0.75, size=4) + 
		geom_abline();

# > g <- ggplot(coverageFile, aes(x=mean_coverage)) + geom_histogram()
#> mean(coverageFile[,"mean_coverage"])
#[1] 0.5612449
#> median(coverageFile[,"mean_coverage"])
#[1] 0.4483885
