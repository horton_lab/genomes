# This R-script processes the "first pass" in snp calling to recalibrate the bam files
# which then allows a hard-filter approach.
#
# Author: Matt Horton
###############################################################################

rm(list=ls());

baseDirectory <- "/data/home/mhorto/work/vesca/genomes"; ## TODO: switch to env_vars

###############################################################################
## user specific variables
###############################################################################
#sequencingCenter <- "fgcz";
#aliasFileName <- "lane_map.txt"; ##mapFileName
#coreFacilitySummaryFileName <- "theFirst28.txt"; ##fgczDatasetFileName
#resultsDirectory <- "results";
#sequencingCenter <- "novogene"; setwd(paste0( baseDirectory, "/", sequencingCenter ));
#
#aliasFileName <- NA;
#coreFacilitySummaryFileName <- "microbiomeSurvey2017_150genomes.txt";

resultsDirectory <- "results";
sequencingCenter <- "fgcz"; setwd(paste0( baseDirectory, "/", sequencingCenter )); ## "novogene";

aliasFileName <- "lane_map.txt"; ##mapFileName ## NA
coreFacilitySummaryFileName <- "all_lines.txt"; ##"theFirst28.txt"; "microbiomeSurvey2017_150genomes.txt";

###############################################################################
## specify the reference to align against (and its location)
###############################################################################
referenceDirectory <- paste0( baseDirectory, "/reference" );
referenceFile <- paste0(referenceDirectory, "/Fragaria_vesca_v4.0.a1.fa");
stopifnot(dir.exists(referenceDirectory) & file.exists(referenceFile));

###############################################################################
## our internal alias file, which maps core facility ids to our internal sample ids
###############################################################################
if( !is.na(aliasFileName)){
	aliasFile <- read.table(aliasFileName, header=T, sep="\t", as.is=T, stringsAsFactors=FALSE);	
	
	coreSampleIdColumnName <- "fgcz_sample_name";
	ourSampleIdColumnName <- "mh_id";
	stopifnot((coreSampleIdColumnName %in% colnames(aliasFile)) & (ourSampleIdColumnName %in% colnames(aliasFile)));
	
} else {
	aliasFile <- NA;
}

###############################################################################
## the file that attaches sample ids to r1 and r2
###############################################################################
coreFacilitySummaryFile <- read.table(coreFacilitySummaryFileName, header=T, sep="\t", as.is=T, stringsAsFactors=FALSE); ##fgczFile
coreFacilitySummaryFile <- cbind(coreFacilitySummaryFile, rg1=rep("", nrow(coreFacilitySummaryFile)), rg2=rep("", nrow(coreFacilitySummaryFile)), stringsAsFactors=FALSE);

writeJobToCreateBqsrReports <- function( bamFileDirectory, markedBamFiles, referenceFile, goldishSnpStandard="pre_calls_all_filtsnps.vcf", goldishIndelStandard="pre_calls_all_filtindels.vcf", memoryInGb=12 ){
	
	cat("Writing the base recalibrator shell script now.\n\n");
	cat("Processing:", length(markedBamFiles), "deduplicated bam files.\n\n");
	
	scriptDirectory <- "code/";
	if( !dir.exists(scriptDirectory)){
		result <- dir.create( scriptDirectory );
	}

	filename <- paste0( scriptDirectory, "local_baserecalibrator___.sh");
	
	qsubString <- paste0(
			"#!/bin/bash\n",
			"cd ../", bamFileDirectory, "\n",
			"declare -a filenames=(" ); 

	for( j in 1:length(markedBamFiles) ){
		qsubString <- paste0(qsubString, "\"", markedBamFiles[[j]], "\"");

		if( j < length(markedBamFiles)){
			qsubString <- paste0( qsubString, " ");

		} else {
			qsubString <- paste0( qsubString, ")\n");
		}
	}

	qsubString <- paste0(qsubString, "\necho \"Now processing: ${filenames[$1]}\"\n");
	qsubString <- paste0(qsubString, "deduped_bamfile=${filenames[$1]}\n");
	qsubString <- paste0(qsubString, "output_recal_table=${deduped_bamfile}_recal.table\n\n");

	## sort in coordinate order
	qsubString <- paste0(qsubString, "gatk --java-options \"-Xmx", memoryInGb, "G\" BaseRecalibrator \\\n",
			" --input ${deduped_bamfile} \\\n",
			" --output ${output_recal_table} \\\n",
			" --reference ", referenceFile, " \\\n",
			" --known-sites ", goldishSnpStandard, " \\\n",
			" --known-sites ", goldishIndelStandard, " \\\n",
			" --tmp-dir ${XDG_RUNTIME_DIR} \\\n",
			" 2>logs/calls/bqsr_report_${deduped_bamfile}.out\n");

	qsubString <- paste0(qsubString, "\necho \"${deduped_bamfile} was analyzed with base recalibrator, the recalibrated table is: ${output_recal_table}\"\n");

	sink(filename);
	cat(qsubString);
	sink(NULL);
}

dedupedBamFiles <- list();
for( j in 1:nrow(coreFacilitySummaryFile)){
	
	if( !is.null( aliasFile )){
		sampleInfo <- aliasFile[which(aliasFile[,coreSampleIdColumnName] == coreFacilitySummaryFile[j, "Name"]), ourSampleIdColumnName];
		
	} else {
		sampleInfo <- coreFacilitySummaryFile[j, "Name"];
	}
	
	bamFile <- paste0( sampleInfo, ".bam.dedup" );
	stopifnot(file.exists(paste0(resultsDirectory, "/", bamFile)));
	
	dedupedBamFiles[[sampleInfo]] <- bamFile;
}

writeJobToCreateBqsrReports(bamFileDirectory=resultsDirectory, markedBamFiles=dedupedBamFiles, referenceFile=referenceFile, memoryInGb=36 );









