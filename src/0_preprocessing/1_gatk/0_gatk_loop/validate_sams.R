# This R-script checks the format of our bam files
# 
# Author: Matt Horton
###############################################################################

rm(list=ls());

baseDirectory <- "/data/home/mhorto/work/vesca/genomes";

###############################################################################
## user specific variables
###############################################################################
#sequencingCenter <- "fgcz";
#aliasFileName <- "lane_map.txt"; ##mapFileName
#coreFacilitySummaryFileName <- "theFirst28.txt"; ##fgczDatasetFileName
analyticalOutputDirectory <- "results";
sequencingCenter <- "novogene"; setwd(paste0( baseDirectory, "/", sequencingCenter ));
aliasFileName <- NA;

coreFacilitySummaryFileName <- "microbiomeSurvey2017_150genomes.txt";

###############################################################################
## specify the reference to align against (and its location)
###############################################################################
useReference <- TRUE;

referenceDirectory <- paste0( baseDirectory, "/reference" );
referenceFile <- paste0(referenceDirectory, "/Fragaria_vesca_v4.0.a1.fa");
stopifnot(dir.exists(referenceDirectory) & file.exists(referenceFile));

###############################################################################
## our internal alias file, which maps core facility ids to our internal sample ids
###############################################################################
if( !is.na(aliasFileName)){
	aliasFile <- read.table(aliasFileName, header=T, sep="\t", as.is=T, stringsAsFactors=FALSE);	
	
	coreSampleIdColumnName <- "fgcz_sample_name";
	ourSampleIdColumnName <- "mh_id";
	stopifnot((coreSampleIdColumnName %in% colnames(aliasFile)) & (ourSampleIdColumnName %in% colnames(aliasFile)));

} else {
	aliasFile <- NA;
}

###############################################################################
## the file that attaches sample ids to r1 and r2
###############################################################################
coreFacilitySummaryFile <- read.table(coreFacilitySummaryFileName, header=T, sep="\t", as.is=T, stringsAsFactors=FALSE); ##fgczFile
coreFacilitySummaryFile <- cbind(coreFacilitySummaryFile, rg1=rep("", nrow(coreFacilitySummaryFile)), rg2=rep("", nrow(coreFacilitySummaryFile)), stringsAsFactors=FALSE);

writeValidateSamScript <- function( bamDirectory, bamfiles, filename, reference ){
	
	cat("Writing the validate bam file shell script now.\n\n");
	scriptDirectory <- "code/";
	if( !dir.exists(scriptDirectory)){
		result <- dir.create( scriptDirectory );
	}
	
	## need to recode this to be a loop in the sh itself rather than using xargs to iterate over files.
	filename <- paste0( scriptDirectory, "local_validatebam_", gsub(".txt", "", filename), ".sh");
	
	qsubString <- paste0(
			"#!/bin/bash\n",
			"cd ../", bamDirectory, "\n",
			"declare -a filenames=(" ); 
	
	for( j in 1:length(bamfiles)){
		qsubString <- paste0(qsubString, "\"", bamfiles[[j]], "\"");

		if( j < length(bamfiles)){
			qsubString <- paste0( qsubString, " ");

		} else {
			qsubString <- paste0( qsubString, ")\n");
		}
	}

	qsubString <- paste0(qsubString, "gatk ValidateSamFile \\\n", 
							" --INPUT ${filenames[$1]} \\\n" );

	if( !is.na( reference )){
		qsubString <- paste0( qsubString, " --REFERENCE_SEQUENCE \"", reference, "\" ;")
	}

	qsubString <- paste0( qsubString, " > logs/validation_logs/${filenames[$1]}", ifelse( is.na(reference), "", ".ref"), ".log\n");

	sink(filename);
	cat(qsubString);
	sink(NULL);
}

bamFiles <- list();
for( j in 1:nrow(coreFacilitySummaryFile)){

	if( !is.na( aliasFile )){
		sampleInfo <- aliasFile[which(aliasFile[,coreSampleIdColumnName] == coreFacilitySummaryFile[j, "Name"]), ourSampleIdColumnName];

	} else {
		sampleInfo <- coreFacilitySummaryFile[j, "Name"];
	}

	bamFile <- paste0( sampleInfo, ".bam" );
	stopifnot(file.exists(paste0(analyticalOutputDirectory, "/", bamFile)));

	bamFiles[[sampleInfo]] <- bamFile;
}

writeValidateSamScript( bamDirectory=analyticalOutputDirectory, bamfiles=bamFiles, filename=coreFacilitySummaryFileName, reference=ifelse( useReference, referenceFile, NA ));
