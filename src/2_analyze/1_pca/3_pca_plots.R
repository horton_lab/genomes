# This R-script plots the output from pca or smartpca...
# TODO: Add comments...
#
# Author: Matt Horton
###############################################################################

rm(list=ls());

require(ggplot2);

source(paste0(Sys.getenv('VESCA_GENOME_CODE'), "hdr.genomic.methods.R"));
source(paste0(Sys.getenv('GENERAL_CODE'), "hdr.base_methods.R"));

###############################################################################
## user variables
###############################################################################
proportionsToTest <- c( 0, 0.01, 0.025, 0.05, 0.1, 0.25, 0.5, 0.75 );
subProject <- "survey";
filenamePrefix <- "Europe.gdb_survey_filtsnps";

{
	###############################################################################
	## move to the data directory
	###############################################################################
	sourceDirectory <- paste0( Sys.getenv('OUTPUT_DIR2'), "/kinship/", subProject, "/pca/plink/" );
	stopifnot( dir.exists(sourceDirectory)); 
	setwd( sourceDirectory );

	outputDirectory <- paste0( Sys.getenv('OUTPUT_DIR2'), "images/kinship/", subProject, "/pca/plink/" );
	if( !dir.exists(outputDirectory)){
		dir.create(outputDirectory, recursive=TRUE);
		cat("Created output directory:", outputDirectory, "\n");
	}
	
	###############################################################################
	## 
	###############################################################################
	for( j in 1:length(proportionsToTest)){
		prop <- proportionsToTest[j];
		
		subDirectory <- paste0( "miss_", prop );
		stopifnot(dir.exists(subDirectory));

		setwd(subDirectory);
		pcaFile <- paste0( filenamePrefix, ifelse( prop != 1, "_na", "_" ), prop, ".pcs.txt");

		if( !file.exists( pcaFile )){
			smartPcaFile <- gsub( ".pcs.txt$", ".evec", pcaFile );
			stopifnot( file.exists( smartPcaFile ));

			command <- paste0( "sed -e 's/^  *//;s/  *$//;s/  */\t/g' ", smartPcaFile, " > tmp && mv tmp ", pcaFile );
			return <- system( command, intern=T, wait=T);

			stopifnot( return == 0 );
		}
		
		###############################################################################
		## prepare the PCs
		###############################################################################
		pcs <- read.table(pcaFile, header=F, as.is=T, stringsAsFactors=FALSE, comment.char="", row.names=1, fill=TRUE );
		eigenvalues <- pcs[1,];
		eigenvalues <- eigenvalues^2; ##
		proportionOfVariance <- 100 * round(eigenvalues/sum(eigenvalues, na.rm=T), digits=3)[1:2]; ## express as a % of variance

		pcs <- pcs[-1,1:5];
		colnames(pcs) <- paste0("PC", 1:ncol(pcs));

		###############################################################################
		## plot the first 2 PCs...
		###############################################################################
		xlabText <- paste0("PC1 (", proportionOfVariance[1], "%)");
		ylabText <- paste0("PC2 (", proportionOfVariance[2], "%)");

		g <- ggplot( pcs, aes(x=PC1, y=PC2)) + 
				theme_classic(base_size=20) + 
				geom_vline(xintercept=0, linetype=3, lwd=1, colour="grey") + 
				geom_hline(yintercept=0, linetype=3, lwd=1, colour="grey") +
				labs(x=xlabText, y=ylabText) +  
				geom_point(alpha=0.6, size=5, col="tan1");

		pdfOutputFileName <- paste0( outputDirectory, "/", subDirectory, "/", gsub( ".txt", ".pdf", pcaFile ));

		if( !dir.exists(dirname(pdfOutputFileName))){
			dir.create(dirname(pdfOutputFileName));
		}

		ggsave(g, file=pdfOutputFileName, device="pdf", scale=1, width=8, height=8, useDingbats=FALSE);

		###############################################################################
		## back up a directory...
		###############################################################################
		setwd("../");
	}; 
}
