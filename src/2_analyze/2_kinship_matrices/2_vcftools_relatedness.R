# This R-script creates a kinship matrix using vcftools (Yang or Manichuak)
#
# Author: Matt Horton
###############################################################################

rm(list=ls());

library(pheatmap);

source(paste0(Sys.getenv('VESCA_GENOME_CODE'), "hdr.genomic.methods.R"));
source(paste0(Sys.getenv('GENERAL_CODE'), "hdr.base_methods.R"));

###############################################################################
## user variables
###############################################################################
proportionsToTest <- c( 0, 0.01, 0.025, 0.05, 0.1, 0.25 );
subProject <- "survey";
yangEtAllMatrix <- TRUE;

{
	###############################################################################
	## when necessary, create the output directory/ies
	###############################################################################
	outputDirectory <- paste0( Sys.getenv('OUTPUT_DIR2'), "/kinship/", subProject, "/ibs/vcf/" );
	if( !dir.exists(outputDirectory)){
		dir.create(outputDirectory, recursive=TRUE);
		cat("Created output directory:", outputDirectory, "\n");
	}
	
	###############################################################################
	## identify the parent/source vcf
	###############################################################################
	dataDirectory <- paste0( Sys.getenv('VESCA_GENOMES') );
	setwd( dataDirectory );
	
	vcfs <- chooseFiles( pattern=".vcf.gz$", numberOfChoices=1 );
	vcfFile <- vcfs$filename;
	
	###############################################################################
	## using vcftools, extract the requested SNPs
	###############################################################################
	results <- list();
	for( j in 1:length(proportionsToTest)){
		prop <- proportionsToTest[j];
		cat("Processing file:", vcfFile, "with --max-missing of", (1-prop), "\n");
		
		outputFileName=paste0( normalizePath( outputDirectory ), "/miss_", prop, "/", gsub(".vcf.gz", "", vcfFile), ifelse( prop != 1, "_na", "_" ), prop);
		if( !dir.exists(dirname(outputFileName))){ 
			dir.create(dirname(outputFileName));
			cat("Created output directory:", dirname(outputFileName), "\n");
		}
		
		###############################################################################
		## create the kinship matrix... 
		###############################################################################
		cat("The output file name (by prefix) will be:", outputFileName, "\n");
		timer_j <- system.time( 
				result_j <- 
						vcfKinship( outputName=outputFileName, 
						vcf=vcfFile, proportionMissingData=prop, yang=yangEtAllMatrix ));

		file_j <- read.table( result_j$filename, header=T, sep="\t", as.is=T, stringsAsFactors=FALSE );

		samples <- unique( file_j[,"INDV1"] );
		kinship <- matrix( nrow=length(samples), ncol=length(samples), dimnames=list( samples, samples ));

		for( k in 1:nrow(file_j)){ ## this loads in the upper triangle
			kinship[file_j[k, "INDV1"], file_j[k,"INDV2"]] <- file_j[k, ifelse(yangEtAllMatrix, "RELATEDNESS_AJK", "RELATEDNESS_PHI")];
		}

		kinship[lower.tri(kinship)] <- t(kinship)[lower.tri(kinship)];
		outputMatrixFileName <- paste0( result_j$filename, ".kin", ifelse(yangEtAllMatrix, "", "g" ), "_mat.txt" );
		write.table( kinship, outputMatrixFileName, quote=F, sep="\t" );

		outputMatrixFileName <- gsub( ".txt", ".pdf", outputMatrixFileName );
		pdf( outputMatrixFileName, width=7, height=7, onefile=FALSE);
		pmap <- pheatmap( kinship, 
						col=viridis::viridis(10), 
						cluster_rows=FALSE, 
						cluster_cols=FALSE,
						fontsize=5 );

		dev.off();
	}
}
